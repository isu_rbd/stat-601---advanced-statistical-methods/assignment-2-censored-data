BPD Oxygen data

From: Hosmer, D.W. and Lemeshow, S. and May, S. (2008) Applied Survival
Analysis: Regression Modeling of Time to Event Data: Second Edition, John Wiley
and Sons Inc., New York, NY

These data are used in Chapter 2 of the book.

Description:
Low birth weight infants (<1500 grams) with bron-chopulmonary dsyplasia (BPD) are pur on oxygen.  The data give the number of days such infants wer on oxygen. The data were collected retrospectively for the period December 1987 to March 1991.  Beginning in August 1989, the treatment of BPD changed to include the use of a surfactant replacement therapy.  This required parental consent since, at the time, it was considered experimental.  
A total of 78 infants met the study criteria, with 35 receiving the surfactant replacement therapy and 43 not receiving this therapy.  Five infants were still on oxygen at their last follow-up visit and are censored observations.  

The outcome variable is the number of days the babies required supplemental oxygen therapy.  

Data are in the file bpddat.txt

n = 78

Variables in the data set are:

Variable   Description              Codes/Values
id         Study ID                 1 - 78
surfact    Surfactant use           0 = No, 1 = Yes
ondays     Days in Oxygen           Days
censor     Censoring Indicator      1 = Off Oxygen
                                    0 = Still on Oxygen
                                        at Study End